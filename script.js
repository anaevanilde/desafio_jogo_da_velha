// declaração de variáveis usadas em mais de uma função
var jogador=0;
var resultado="nada";


// Realiza uma jogada
function selecionar(botao) {
     //esta primeira condicional é para impedir que os jogadores continuem marcando posições no jogo depois que ele acabe
     if(resultado=="nada"){
          //esta condicional verificar se a posição já está preenchida, se estiver mostra um alerta para o jogador
          if(botao.innerHTML=="X" || botao.innerHTML=="O"){
               alert("Essa posição já foi preenchida");
          }
          else{
               //esta condicional direciona a vez para cada jogador (um certo veterano me ensinou que % fazem os valores circularem)
               if((jogador%2)+1==1){
                    botao.innerHTML="X";
               }
               else if ((jogador%2)+1==2) {
                    botao.innerHTML="O";
               }
               //chamando a função que verifica se há ganhador ou empatou
               ganhou();
               //testa o resultado que foi testado na funcao acima e se o jogo tiver acabado exibe no html o resultado
               if(resultado=="X"){
                    document.getElementById("indicadorVencedor").innerHTML="O jogador 1 venceu!";
               }
               else if(resultado=="O"){
                    document.getElementById("indicadorVencedor").innerHTML="O jogador 2 venceu!";
               }
               else if (resultado=="EMPATE") {
                    document.getElementById("indicadorVencedor").innerHTML="Velha!";
               }
               //passa a vez do jogador
               jogador++;
               //exibe o jogador da vez
               document.getElementById("indicadorDaVez").innerHTML=(jogador%2)+1;
          }
     }
}

// Zera todos as posições e recomeça o jogo
function resetar(){
     //zera as posições e tira as bordas verdes
     for(var i=0;i<9;i++){
          document.getElementsByClassName("casa")[i].innerHTML="";
          document.getElementsByClassName("casa")[i].style.borderColor="";
     }
     //reinicia o contador de controle vez
     jogador=0;
     //escreve o jogador padrão que começa, garantido que não vai ficar o último da partida anteirior
     document.getElementById("indicadorDaVez").innerHTML="1";
     //deixa o espaço que indica o vencedor vazio, até porque não tem
     document.getElementById("indicadorVencedor").innerHTML="<br>";
     //zera o resultado para garantir que não trave na condição que testa se o jogo acabou na função selecionar
     resultado="nada";
}

// Verifica todos as combinações possíveis de se ganhar o jogo
function ganhou() {
     var cont=0;
     var posicao=document.getElementsByClassName("casa");
     //verificando se deu Empate
     for(var i=0;i<9;i++){
          if(posicao[i].innerHTML=="X" || posicao[i].innerHTML=="O"){
               cont++;
          }
     }
     if(cont==9){
          resultado="EMPATE"
     }

     //verificando se alguem ganhou nas linhas

     //verificando coluna se X ou O ganhou na linha 1
     if((posicao[0].innerHTML=="X" && posicao[1].innerHTML=="X") && posicao[2].innerHTML=="X" ){
          resultado="X";
          //muda a cor da borda onde o jogo foi ganhado
          posicao[0].style.borderColor= "green";
          posicao[1].style.borderColor= "green";
          posicao[2].style.borderColor= "green";
     }
     if((posicao[0].innerHTML=="O" && posicao[1].innerHTML=="O") && posicao[2].innerHTML=="O" ){
          resultado="O";
          //muda a cor da borda onde o jogo foi ganhado
          posicao[0].style.borderColor= "green";
          posicao[1].style.borderColor= "green";
          posicao[2].style.borderColor= "green";
     }
     //verificando coluna se X ou O ganhou na linha 2
     if((posicao[3].innerHTML=="X" && posicao[4].innerHTML=="X") && posicao[5].innerHTML=="X" ){
          resultado="X";
          //muda a cor da borda onde o jogo foi ganhado
          posicao[3].style.borderColor= "green";
          posicao[4].style.borderColor= "green";
          posicao[5].style.borderColor= "green";
     }
     if((posicao[3].innerHTML=="O" && posicao[4].innerHTML=="O") && posicao[5].innerHTML=="O" ){
          resultado="O";
          //muda a cor da borda onde o jogo foi ganhado
          posicao[3].style.borderColor= "green";
          posicao[4].style.borderColor= "green";
          posicao[5].style.borderColor= "green";
     }
     //verificando coluna se X ou O ganhou na linha 3
     if((posicao[6].innerHTML=="X" && posicao[7].innerHTML=="X") && posicao[8].innerHTML=="X" ){
          resultado="X";
          //muda a cor da borda onde o jogo foi ganhado
          posicao[6].style.borderColor= "green";
          posicao[7].style.borderColor= "green";
          posicao[8].style.borderColor= "green";
     }
     if((posicao[6].innerHTML=="O" && posicao[7].innerHTML=="O") && posicao[8].innerHTML=="O" ){
          resultado="O";
          //muda a cor da borda onde o jogo foi ganhado
          posicao[6].style.borderColor= "green";
          posicao[7].style.borderColor= "green";
          posicao[8].style.borderColor= "green";
     }


     //verificando se alguem ganhou nas colunas

     //verificando coluna se X ou O ganhou na coluna 1
     if((posicao[0].innerHTML=="X" && posicao[3].innerHTML=="X") && posicao[6].innerHTML=="X" ){
          resultado="X";
          //muda a cor da borda onde o jogo foi ganhado
          posicao[0].style.borderColor= "green";
          posicao[3].style.borderColor= "green";
          posicao[6].style.borderColor= "green";

     }
     if((posicao[0].innerHTML=="O" && posicao[3].innerHTML=="O") && posicao[6].innerHTML=="O" ){
          resultado="O";
          //muda a cor da borda onde o jogo foi ganhado
          posicao[0].style.borderColor= "green";
          posicao[3].style.borderColor= "green";
          posicao[6].style.borderColor= "green";
     }
     //verificando coluna se X ou O ganhou na coluna 2
     if((posicao[1].innerHTML=="X" && posicao[4].innerHTML=="X") && posicao[7].innerHTML=="X" ){
          resultado="X";
          //muda a cor da borda onde o jogo foi ganhado
          posicao[1].style.borderColor= "green";
          posicao[4].style.borderColor= "green";
          posicao[7].style.borderColor= "green";
     }
     if((posicao[1].innerHTML=="O" && posicao[4].innerHTML=="O") && posicao[7].innerHTML=="O" ){
          resultado="O";
          //muda a cor da borda onde o jogo foi ganhado
          posicao[1].style.borderColor= "green";
          posicao[4].style.borderColor= "green";
          posicao[7].style.borderColor= "green";
     }
     //verificando coluna se X ou O ganhou na coluna 3
     if((posicao[2].innerHTML=="X" && posicao[5].innerHTML=="X") && posicao[8].innerHTML=="X" ){
          resultado="X";
          //muda a cor da borda onde o jogo foi ganhado
          posicao[2].style.borderColor= "green";
          posicao[5].style.borderColor= "green";
          posicao[8].style.borderColor= "green";
     }
     if((posicao[2].innerHTML=="O" && posicao[5].innerHTML=="O") && posicao[8].innerHTML=="O" ){
          resultado="O";
          //muda a cor da borda onde o jogo foi ganhado
          posicao[2].style.borderColor= "green";
          posicao[5].style.borderColor= "green";
          posicao[8].style.borderColor= "green";
     }


     //verificando se alguem ganhou nas diagonais

     //verificando coluna se X ou O ganhou na diagonal principal
     if((posicao[0].innerHTML=="X" && posicao[4].innerHTML=="X") && posicao[8].innerHTML=="X" ){
          resultado="X";
          //muda a cor da borda onde o jogo foi ganhado
          posicao[0].style.borderColor= "green";
          posicao[4].style.borderColor= "green";
          posicao[8].style.borderColor= "green";
     }
     if((posicao[0].innerHTML=="O" && posicao[4].innerHTML=="O") && posicao[8].innerHTML=="O" ){
          resultado="O";
          //muda a cor da borda onde o jogo foi ganhado
          posicao[0].style.borderColor= "green";
          posicao[4].style.borderColor= "green";
          posicao[8].style.borderColor= "green";
     }
     //verificando coluna se X ou O ganhou na diagonal secundaria
     if((posicao[2].innerHTML=="X" && posicao[4].innerHTML=="X") && posicao[6].innerHTML=="X" ){
          resultado="X";
          //muda a cor da borda onde o jogo foi ganhado
          posicao[2].style.borderColor= "green";
          posicao[4].style.borderColor= "green";
          posicao[6].style.borderColor= "green";
     }
     if((posicao[2].innerHTML=="O" && posicao[4].innerHTML=="O") && posicao[6].innerHTML=="O" ){
          resultado="O";
          //muda a cor da borda onde o jogo foi ganhado
          posicao[2].style.borderColor= "green";
          posicao[4].style.borderColor= "green";
          posicao[6].style.borderColor= "green";
     }
}
